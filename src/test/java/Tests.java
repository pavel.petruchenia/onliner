import com.google.common.base.Verify;

import org.testng.Assert;
import org.testng.annotations.*;
import pages.TVPage;
import pages.HomePage;

import java.io.IOException;

public class Tests extends BaseTest {
    final String loginPageTitle = "Onliner";
    final String HEADER_TITLE = "Телевизоры";

    @Test
    public void correctTitleOnMainPage() {
        HomePage main = new HomePage();
        final String pageTitle = main.getPageTitle();
        Verify.verify(pageTitle.equals(loginPageTitle), "The title is correct");
    }

    @Test
    public void currentUrl() throws IOException {
        HomePage main = new HomePage();
        final String pageUrl = main.getPageUrl();
        Assert.assertEquals(pageUrl, propertyValues.getPropertyValue("baseUrl"));
    }

    @Test
    public void openTVTab() {
        HomePage main = new HomePage();
        main.openTVTab();
        TVPage tvPage = new TVPage();
        String subTitle = tvPage.subTitle();
        Assert.assertEquals(subTitle, HEADER_TITLE, "The header title is incorrect");
    }

    @Test
    public void checkboxUnchecked() {
        HomePage main = new HomePage();
        main.openTVTab();
        TVPage tvPage = new TVPage();
        tvPage.checkCheckbox("samsung", "Samsung");
        String subTitle = tvPage.subTitle();
        Verify.verify(HEADER_TITLE.equals(subTitle), "The title is incorrect");
        Assert.assertFalse(tvPage.checkBoxCondition("samsung"), "Samsung checkbox is unchecked");
    }

    @Test
    public void checkOtherProducer() {
        HomePage main = new HomePage();
        main.openTVTab();
        TVPage tvPage = new TVPage();
        tvPage.checkCheckbox("samsung", "Samsung");
        Assert.assertFalse(tvPage.checkBoxCondition("samsung"), "Samsung checkbox is unchecked");
        tvPage.checkCheckbox("sony", "Sony");
        Assert.assertTrue(tvPage.checkBoxCondition("sony"), "Sony checkbox unchecked");
        Assert.assertEquals(tvPage.tagText(), "Sony", "The producer doesn't match selected value");
    }

}
