import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import utils.WebDriverManager;

import java.io.IOException;

public class BaseTest {
    static PropertyValues propertyValues = new PropertyValues();
    @BeforeMethod
    public static void setUp() throws IOException {
        WebDriverManager.getDriver().manage().window().maximize();
        WebDriverManager.getDriver().get(propertyValues.getPropertyValue("baseUrl"));
        System.out.println("Successfully followed the link");
    }

    @AfterMethod
    public void tearDown() {
        WebDriverManager.stopDriver();
        System.out.println("The close_up process is completed");
    }
}
