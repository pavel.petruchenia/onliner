package utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyValues {
    InputStream inputStream;

    public String getPropertyValue(String key) throws IOException {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");

            inputStream.close();
            return prop.getProperty(key);
    }

}