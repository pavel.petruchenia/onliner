package utils;

import org.openqa.selenium.By;

public class Locators {

    public static class HomePage {
        public static final By TV = By.xpath("//*[text()= 'Телевизоры']");
    }
    public static class TVPageLoc {

        public static final By SUBTITLE = By.xpath("//h1[@class='schema-header__title']");
        public static final String CHECKBOX_TV_CONDITION = ("//ul[@class='schema-filter__list']//input[@value='%s']");
        public static final String CHECKBOX_TV = ("//ul[@class='schema-filter__list']//span[text()='%s']");
        public static final By TAGS_TEXT = By.xpath("//*[@class='schema-tags__text']");
    }
}
