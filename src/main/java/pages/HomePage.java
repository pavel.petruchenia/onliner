package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Text;
import utils.Locators;
import utils.WebDriverManager;

public class HomePage extends BasePage {
    public HomePage() {
    }

    public String getPageUrl() {
        return WebDriverManager.getDriver().getCurrentUrl();
    }

    public String getPageTitle() {
        return WebDriverManager.getDriver().getTitle();
    }

    public void openTVTab() {
        getElement(Locators.HomePage.TV).click();
        System.out.println("TV tab was clicked");
    }
}
