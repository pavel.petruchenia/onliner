package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import utils.WebDriverManager;

import java.time.Duration;
import java.util.function.Function;

public class BasePage {
    public static WebDriver driver;

    public BasePage() {
        this.driver = WebDriverManager.getDriver();
    }

    public static WebElement getElement(By bySomething) {
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofMillis(5))
                .ignoring(NoSuchElementException.class)
                .withMessage("Element has not been found");

        WebElement fluentWait = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {return driver.findElement(bySomething);}
        });
        return fluentWait;
    }
}
