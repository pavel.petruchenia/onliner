package pages;

import org.openqa.selenium.By;
import utils.Locators;

import java.util.Scanner;

public class TVPage extends BasePage {
    public TVPage() {
    }

    public String subTitle() {
        return getElement(Locators.TVPageLoc.SUBTITLE).getText();
    }

    public void checkCheckbox(String inputName, String checkboxName) {
        if (checkBoxCondition(inputName)) {
            getElement(By.xpath(String.format(Locators.TVPageLoc.CHECKBOX_TV, checkboxName))).click();
            System.out.println("CheckBox is selected");
        } else {
            getElement(By.xpath(String.format(Locators.TVPageLoc.CHECKBOX_TV, checkboxName))).click();
            System.out.println("else CheckBox is unchecked");
        }
    }

    public boolean checkBoxCondition(String checkboxName) {
        return getElement(By.xpath(String.format(Locators.TVPageLoc.CHECKBOX_TV_CONDITION, checkboxName))).isSelected();
    }

    public String tagText() {
        return getElement(Locators.TVPageLoc.TAGS_TEXT).getText();
    }
}
